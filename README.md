## 💻 Sobre o projeto

Projeto para gerenciamento de plantas indoor ou outdoor


## 🛠 Tecnologias

As seguintes tecnologias foram utilizadas no desenvolvimento da API Rest do projeto:

- **[Java 17](https://www.oracle.com/java)**
- **[Spring Boot 3](https://spring.io/projects/spring-boot)**
- **[Maven](https://maven.apache.org)**
- **[MySQL](https://www.mysql.com)**
- **[Hibernate](https://hibernate.org)**
- **[Flyway](https://flywaydb.org)**
- **[Lombok](https://projectlombok.org)**

---

# Spring Security

- Autenticação
- Autorização (controle de acesso)
- Proteção contra-ataques (CSRF, clickjacking, etc.)

Em suma, o Spring Security possui três objetivos. Um deles é providenciar um serviço para customizarmos como será o controle de autenticação no projeto. Isto é, como os usuários efetuam login na aplicação.

O Spring Security possui, também, a autorização, sendo o controle de acesso para liberarmos a requisição na API ou para fazermos um controle de permissão.


*Esse é o conceito de Stateful:*

Toda vez que um usuário efetua o login em uma aplicação Web, o servidor armazena o estado. Isto é, cria as sessões e, com isso, consegue identificar cada usuário nas próximas requisições.

Por exemplo, esse usuário é dono de determinada sessão, e esses são os dados de memória deste usuário. Cada usuário possui um espaço na memória. Portanto, o servidor armazena essas sessões, espaços em memória e cada sessão contém os dados específicos de cada usuário.

Porém, em uma API Rest, não deveríamos fazer isso, porque um dos conceitos é que ela seja stateless, não armazena estado. Caso o cliente da API dispare uma requisição, o servidor processará essa requisição e devolverá a resposta.


# Spring Security Configuration

Dentro da classe *SecurityConfigurations* , vamos incluir a configuração do processo de autenticação, que precisa ser stateless. Para isso, criaremos um método cujo retorno será um objeto chamado SecurityFilterChain, do próprio Spring.

O objeto SecurityFilterChain do Spring é usado para configurar o processo de autenticação e de autorização.


**Description: Field manager in med.voll.api.controller.AutenticacaoController required a bean of type 'org.springframework.security.authentication.AuthenticationManager' that could not be found.**

Isso significa que o campo manager na classe Autenticacao Controller requer um bean do tipo Authentication Manager, que não pôde ser encontrado.

Isto é, no momento de carregar o AutenticacaoController, ele não encontrou o Authentication Manager. Não conseguiu injetar o atributo manager na classe controller.

A classe AuthenticationManager é do Spring. Porém, ele não injeta de forma automática o objeto AuthenticationManager, precisamos configurar isso no Spring Security. Como não configuramos, ele não cria o objeto AuthenticationManager e lança uma exceção.

Por ser uma configuração de segurança, faremos essa alteração na classe SecurityConfigurations.

A classe AuthenticationConfiguration, possui o método getAuthenticationManager() que cria o objeto AuthenticationManager.

Esse é o método que estamos informando ao Spring como injetar objetos. Portanto, acima dele incluiremos a anotação @Bean.


###  Algoritmo de hashing de senhas

Para gerarmos o hashing da senha e salvá-la na coluna "senha" da tabela. Neste projeto, usamos o algoritmo BCrypt.

#  Spring Security configuração de Usuário

Para o Spring Security identificar a classe usuário do nosso projeto, precisamos informar. Por exemplo, como ele vai saber que o atributo login é o campo login? A forma para identificarmos isso é usando uma interface.

Portanto, precisamos implementar uma interface chamada UserDetails (própria do Spring Security) na classe que representa o usuário.

No isCredentialsNonExpired(), é só caso quisermos controlar a conta do usuário: se há uma data de expiração ou se pode ter as credenciais bloqueadas. No caso, não faremos esse controle de conta, portanto, vamos devolver tudo como verdadeiro.

Isso para comunicar ao Spring que o usuário não está bloqueado, está habilitado e a conta não expirou. Assim, retornamos tudo true.

No primeiro método criado, precisamos devolver um objeto do tipo Collection chamado getAuthorities. Caso tenhamos um controle de permissão no projeto, por exemplo, perfis de acesso, é necessário criar uma classe que represente esses perfis.

No nosso caso, não controlamos os perfis. Se o usuário estiver cadastrado, pode acessar qualquer tela sem restrições. Mas precisamos devolver para o Spring uma coleção representando os perfis.

Para isso, vamos simular uma coleção para compilarmos o projeto. Não usaremos, mas devolveremos um objeto válido para o Spring.

No retorno, ao invés de null, vamos inserir List.of(). Dentro do parêntese, criaremos um objeto do tipo new SimpleGrantedAuthority(), sendo a classe do Spring que informa qual o perfil do usuário.

Passaremos um perfil estático, em SimpleGrantedAuthority(). Por padrão, os perfis do Spring possui um prefixo, ROLE_, e o nome do perfil. No caso, será USER.

