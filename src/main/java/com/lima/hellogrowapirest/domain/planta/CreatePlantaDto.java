package com.lima.hellogrowapirest.domain.planta;

import com.lima.hellogrowapirest.domain.grow.Grow;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreatePlantaDto {
    public Long id;
    public String nomePlanta;
    public String especie;
    public Long growId;
    public Grow grow;
}
