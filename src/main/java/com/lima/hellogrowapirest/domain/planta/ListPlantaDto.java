package com.lima.hellogrowapirest.domain.planta;

import com.lima.hellogrowapirest.domain.grow.Grow;
import com.lima.hellogrowapirest.domain.rega.ListRegaDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ListPlantaDto {
    public Long id;
    public String nomePlanta;
    public String especie;
    public Grow grow;
    public List<ListRegaDto> listaRegas = new ArrayList<>();

    public ListPlantaDto(Planta planta) {
        this.id = planta.getId();
        this.nomePlanta = planta.getNomePlanta();
        this.especie = planta.getEspecie();
        this.grow = planta.getGrow();
        if (planta.getListaRegas() != null)
            this.listaRegas = planta.getListaRegas()
                    .stream().map(ListRegaDto::new).collect(Collectors.toList());
    }
}
