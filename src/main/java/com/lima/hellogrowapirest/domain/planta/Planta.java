package com.lima.hellogrowapirest.domain.planta;


import com.lima.hellogrowapirest.domain.grow.Grow;
import com.lima.hellogrowapirest.domain.rega.Rega;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "planta")
public class Planta extends Object {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String nomePlanta;

    @NotNull
    private String especie;

    @ManyToOne
    private Grow grow;

    @OneToMany(mappedBy = "planta")
    private List<Rega> listaRegas = new ArrayList<>();


    public Planta(ListPlantaDto plantaDto) {
        this.id = plantaDto.id;
        this.nomePlanta = plantaDto.nomePlanta;
        this.especie = plantaDto.especie;
        this.grow = plantaDto.grow;
//        this.listaRegas = plantaDto.listaRegas;
    }

    public Planta(CreatePlantaDto plantaDto) {
        this.id = plantaDto.id;
        this.nomePlanta = plantaDto.nomePlanta;
        this.especie = plantaDto.especie;
        this.grow = plantaDto.grow;
    }

    public void atualizaDados(CreatePlantaDto plantaDto) {
        this.nomePlanta = plantaDto.nomePlanta;
        this.especie = plantaDto.especie;
        this.grow = plantaDto.grow;
//        this.listaRegas = plantaDto.listaRegas;
    }
}
