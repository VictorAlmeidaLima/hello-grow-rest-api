package com.lima.hellogrowapirest.domain.user;

public record DadosAutenticacao(String login, String senha) {
}
