package com.lima.hellogrowapirest.domain.grow;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class GrowDto extends Object {

    public Long id;
    public String tamanho;
    public String nomeGrow;



    public GrowDto(Grow grow) {
        this.id = grow.getId();
        this.tamanho = grow.getTamanho();
        this.nomeGrow = grow.getNomeGrow();
    }
}
