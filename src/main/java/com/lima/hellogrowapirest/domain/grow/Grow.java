package com.lima.hellogrowapirest.domain.grow;


import com.lima.hellogrowapirest.domain.grow.GrowDto;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "grow")
public class Grow {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nome_grow")
    String nomeGrow;

    @NotNull
    String tamanho;

    @Override
    public String toString() {
        return "Grow{" +
                "id=" + id +
                ", nomeGrow='" + nomeGrow + '\'' +
                ", tamanho='" + tamanho + '\'' +
                '}';
    }

    public Grow(GrowDto dto){
        this.nomeGrow = dto.nomeGrow;
        this.tamanho = dto.tamanho;
    }

    public void atualizaDados(GrowDto dto){
        if(dto.tamanho != null) this.tamanho = dto.tamanho;
        if(dto.nomeGrow != null) this.nomeGrow = dto.nomeGrow;
    }
}
