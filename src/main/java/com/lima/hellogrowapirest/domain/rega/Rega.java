package com.lima.hellogrowapirest.domain.rega;

import com.lima.hellogrowapirest.domain.planta.Planta;
import com.lima.hellogrowapirest.domain.rega.CreateRegaDto;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "rega")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Rega {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private LocalDate dataRega;

    @NotNull
    @Positive
    private BigDecimal qtdRega;

    @ManyToOne
    private Planta planta;

    public Rega(CreateRegaDto regaDto) {
        this.id = regaDto.id;
        this.dataRega = regaDto.dataRega;
        this.qtdRega = regaDto.qtdRega;
        this.planta = regaDto.planta;
    }
}
