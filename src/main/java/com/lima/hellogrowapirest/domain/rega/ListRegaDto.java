package com.lima.hellogrowapirest.domain.rega;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
public class ListRegaDto {

    public Long id;
    public Long plantaId;
    public BigDecimal qtdRega;
    public LocalDate dataRega;

    public ListRegaDto(Rega rega) {
        this.id = rega.getId();
        this.plantaId = rega.getPlanta().getId();
        this.qtdRega = rega.getQtdRega();
        this.dataRega = rega.getDataRega();

    }

    public ListRegaDto(ListRegaDto listRegaDto) {
        this.id = listRegaDto.id;
        this.plantaId = listRegaDto.plantaId;
        this.qtdRega = listRegaDto.qtdRega;
        this.dataRega = listRegaDto.dataRega;
    }
}
