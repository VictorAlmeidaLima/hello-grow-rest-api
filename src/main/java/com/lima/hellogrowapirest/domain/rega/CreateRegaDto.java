package com.lima.hellogrowapirest.domain.rega;

import com.lima.hellogrowapirest.domain.planta.Planta;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CreateRegaDto {

    public Long id;
    public Long plantaId;
    public Planta planta;
    public BigDecimal qtdRega;
    public LocalDate dataRega;
}
