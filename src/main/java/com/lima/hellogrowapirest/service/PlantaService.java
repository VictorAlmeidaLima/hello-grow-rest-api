package com.lima.hellogrowapirest.service;

import com.lima.hellogrowapirest.domain.planta.Planta;
import com.lima.hellogrowapirest.domain.planta.CreatePlantaDto;
import com.lima.hellogrowapirest.domain.planta.ListPlantaDto;
import com.lima.hellogrowapirest.repository.GrowRepository;
import com.lima.hellogrowapirest.repository.PlantaRepository;
import com.lima.hellogrowapirest.repository.RegaRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlantaService {

    @Autowired
    PlantaRepository plantaRepository;

    @Autowired
    GrowRepository growRepository;

    @Autowired
    RegaRepository regaRepository;


    public Planta salvar(CreatePlantaDto plantaDto) {
        if (plantaDto.growId != null)
            plantaDto.grow = growRepository.getReferenceById(plantaDto.growId);

        Planta planta = new Planta(plantaDto);
        plantaRepository.save(planta);
        return planta;
    }

    public Planta editar(CreatePlantaDto plantaDto) {
        Planta planta = plantaRepository.getReferenceById(plantaDto.id);
        planta.atualizaDados(plantaDto);
        return planta;
    }


    public void deletar(Long id) {
//        ListPlantaDto plantaDto = (ListPlantaDto) obj;
        plantaRepository.deleteById(id);
    }

    public List<ListPlantaDto> listarPlantas(Pageable paginacao) {
      return  plantaRepository.findAll(paginacao).map(ListPlantaDto::new).toList();

    }

    public ListPlantaDto getPlanta(Long id){
        Planta planta = plantaRepository.findById(id).get();
        planta.setListaRegas(regaRepository.findByPlantaId(id));
        return new ListPlantaDto(planta);
    }
}
