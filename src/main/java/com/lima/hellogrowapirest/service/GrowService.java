package com.lima.hellogrowapirest.service;

import com.lima.hellogrowapirest.domain.grow.Grow;
import com.lima.hellogrowapirest.domain.grow.GrowDto;
import com.lima.hellogrowapirest.repository.GrowRepository;
import com.lima.hellogrowapirest.repository.specification.SpecificationGrow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GrowService  {

    @Autowired
    GrowRepository growRepository;


    public Grow salvar(Object obj) {
        GrowDto growDto = (GrowDto) obj;
        Grow grow = new Grow(growDto);
        growRepository.save(grow);
        return grow;
    }


    public Grow editar(Object obj) {
        GrowDto growDto = (GrowDto) obj;
        Grow grow = growRepository.getReferenceById(growDto.id);
        grow.atualizaDados(growDto);
        return grow;
    }


    public void deletar(Long id) {
        growRepository.deleteById(id);

    }

    public List<Grow> listarGrows(GrowDto growDto) {
        return growRepository.findAll(Specification.where(
                SpecificationGrow.nome(growDto.nomeGrow)
                        .or(SpecificationGrow.tamanho(growDto.tamanho))
        ));
    }

    public Grow buscarGrow(Long id) {
        Grow grow = growRepository.findById(id).get();
        return grow;
    }
}
