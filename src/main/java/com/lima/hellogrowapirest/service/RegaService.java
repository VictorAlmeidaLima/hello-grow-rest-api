package com.lima.hellogrowapirest.service;

import com.lima.hellogrowapirest.domain.rega.Rega;
import com.lima.hellogrowapirest.domain.rega.CreateRegaDto;
import com.lima.hellogrowapirest.domain.rega.ListRegaDto;
import com.lima.hellogrowapirest.repository.PlantaRepository;
import com.lima.hellogrowapirest.repository.RegaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RegaService {

    @Autowired
    RegaRepository regaRepository;

    @Autowired
    PlantaRepository plantaRepository;

    public Rega salvar(Object obj) {
        CreateRegaDto regaDto = (CreateRegaDto) obj;
        if(regaDto.plantaId != null)
            regaDto.planta = plantaRepository.getReferenceById(regaDto.plantaId);
        Rega rega = new Rega(regaDto);
        regaRepository.save(rega);
        return rega;
    }

    public void editar(Object obj) {

    }

    public void deletar(Long id) {
        regaRepository.deleteById(id);
    }

    public List<ListRegaDto> listarRegasPorPlanta(Long plantaId) {
        List<Rega> listRegas = regaRepository.findByPlantaId(plantaId);
        List<ListRegaDto> collect = listRegas.stream().map(ListRegaDto::new).collect(Collectors.toList());
        return collect;
    }

    public List<ListRegaDto> listarRegas(Pageable pageable) {
        return regaRepository.findAll(pageable).map(ListRegaDto::new).toList();
    }

}
