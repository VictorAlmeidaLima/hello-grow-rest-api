package com.lima.hellogrowapirest.controller;


import com.lima.hellogrowapirest.domain.planta.Planta;
import com.lima.hellogrowapirest.domain.planta.CreatePlantaDto;
import com.lima.hellogrowapirest.domain.planta.ListPlantaDto;
import com.lima.hellogrowapirest.service.PlantaService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;



import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("planta")
public class PlantaController {

    @Autowired
    PlantaService plantaService;

    @GetMapping
    public ResponseEntity<List<ListPlantaDto>> listarPlantas(@PageableDefault(size = 10,sort = {"nomePlanta"}) Pageable paginacao){
        List<ListPlantaDto> listPlantaDtos = plantaService.listarPlantas(paginacao);
        return ResponseEntity.ok(listPlantaDtos);
    }

    @PutMapping
    @Transactional
    public ResponseEntity<ListPlantaDto> editarPlanta(@RequestBody @Valid CreatePlantaDto plantaDto){
        Planta planta = plantaService.editar(plantaDto);
        return ResponseEntity.ok(new ListPlantaDto(planta));
    }

    @PostMapping
    @Transactional
    public ResponseEntity inserirPlanta(@RequestBody CreatePlantaDto plantaDto, UriComponentsBuilder uriBuilder){
        Planta planta = plantaService.salvar(plantaDto);
        URI uri = uriBuilder.path("/planta/{id}").buildAndExpand(planta.getId()).toUri();
        return  ResponseEntity.created(uri).body(new ListPlantaDto(planta));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ListPlantaDto> getPlanta(@PathVariable Long id){
        ListPlantaDto planta = plantaService.getPlanta(id);
        return ResponseEntity.ok(planta);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletarPlanta(@PathVariable Long id){
        plantaService.deletar(id);
        return ResponseEntity.noContent().build();
    }



}
