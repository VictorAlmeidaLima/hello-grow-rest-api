package com.lima.hellogrowapirest.controller;


import com.lima.hellogrowapirest.domain.rega.Rega;
import com.lima.hellogrowapirest.domain.rega.CreateRegaDto;
import com.lima.hellogrowapirest.domain.rega.ListRegaDto;
import com.lima.hellogrowapirest.service.RegaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("rega")
public class RegaController {

    @Autowired
    RegaService regaService;


    @GetMapping
    public ResponseEntity<List<ListRegaDto>> listarRegas(@PageableDefault(size = 10, sort = "dataRega") Pageable paginacao) {
        List<ListRegaDto> listRegaDtos = regaService.listarRegas(paginacao);
        return ResponseEntity.ok(listRegaDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<ListRegaDto>> getRegas(@PathVariable Long id) {
        List<ListRegaDto> regas = regaService.listarRegasPorPlanta(id).stream().map(ListRegaDto::new).collect(Collectors.toList());
        return ResponseEntity.ok(regas);
    }

    @PostMapping
    @Transactional
    public ResponseEntity inserirRega(@RequestBody CreateRegaDto regaDto, UriComponentsBuilder uriBuilder) {
        Rega rega = regaService.salvar(regaDto);
        URI uri = uriBuilder.path("rega/{id}").buildAndExpand(rega.getId()).toUri();

        return ResponseEntity.created(uri).body(new ListRegaDto(rega));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletaRega(@PathVariable Long id) {
        regaService.deletar(id);
        return ResponseEntity.noContent().build();
    }


}
