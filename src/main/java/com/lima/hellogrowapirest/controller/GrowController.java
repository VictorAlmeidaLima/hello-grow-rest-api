package com.lima.hellogrowapirest.controller;

import com.lima.hellogrowapirest.domain.grow.Grow;
import com.lima.hellogrowapirest.domain.grow.GrowDto;
import com.lima.hellogrowapirest.domain.grow.ListGrowDto;
import com.lima.hellogrowapirest.service.GrowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import jakarta.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("grow")
public class GrowController {


    @Autowired
    GrowService growService;


//    @GetMapping
//    public List<GrowDto> getGrow(@PageableDefault(size = 10,sort = {"nomeGrow"})Pageable paginacao){
//        return growRepository.findAll(paginacao).map(GrowDto::new).toList();
//    }

    @GetMapping
    public ResponseEntity<List<Grow>> filterGrow(@RequestBody @Valid GrowDto growDto) {
        List<Grow> grows = growService.listarGrows(growDto);
        return ResponseEntity.ok(grows);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ListGrowDto> getGrow(@PathVariable Long id) {
        Grow grow = growService.buscarGrow(id);
        return ResponseEntity.ok(new ListGrowDto(grow));
    }

    @PostMapping
    @Transactional
    public ResponseEntity inserirGrow(@RequestBody @Valid GrowDto growDto, UriComponentsBuilder uriBuilder) {

        Grow grow = growService.salvar(growDto);
        URI uri = uriBuilder.path("/grow/{id}").buildAndExpand(grow.getId()).toUri();
        return ResponseEntity.created(uri).body(new ListGrowDto(grow));
    }

    @PutMapping
    @Transactional
    public ResponseEntity<ListGrowDto> alterarGrow(@RequestBody @Valid GrowDto growDto) {

        Grow grow = growService.editar(growDto);
        return ResponseEntity.ok(new ListGrowDto(grow));
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity deleteGrow(@PathVariable Long id) {
        growService.deletar(id);

        return ResponseEntity.noContent().build();
    }


}
