package com.lima.hellogrowapirest.repository;

import com.lima.hellogrowapirest.domain.planta.Planta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantaRepository extends JpaRepository<Planta, Long> {
}
