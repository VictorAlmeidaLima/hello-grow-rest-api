package com.lima.hellogrowapirest.repository;

import java.util.List;

public interface Crud {
    public void salvar(Object obj);
    public void editar(Object obj);
    public void deletar(Long id);
}
