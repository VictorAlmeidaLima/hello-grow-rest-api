package com.lima.hellogrowapirest.repository.specification;

import com.lima.hellogrowapirest.domain.grow.Grow;
import org.springframework.data.jpa.domain.Specification;

public class SpecificationGrow {

    public static Specification<Grow> nome(String nome){
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("nomeGrow"),"%"+nome+"%");
    }

    public static Specification<Grow> tamanho(String tamanho){
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("tamanho"),"%"+tamanho+"%");
    }

}
