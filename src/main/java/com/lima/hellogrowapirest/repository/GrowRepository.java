package com.lima.hellogrowapirest.repository;

import com.lima.hellogrowapirest.domain.grow.Grow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GrowRepository extends JpaRepository<Grow, Long>, JpaSpecificationExecutor<Grow> {

    //Derived query
    List<Grow> findByNomeGrow(String nomeGrow);

    //JPQL
    @Query("SELECT g FROM Grow g where g.nomeGrow like %:nomeGrow%")
    List<Grow> buscarPeloNome(String nomeGrow);

    //NativeQuery
    @Query(value = "SELECT g FROM grow g order by nome_grow", nativeQuery = true)
    List<Grow> buscarTodosOrdenados();
}