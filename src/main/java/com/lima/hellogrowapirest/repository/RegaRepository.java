package com.lima.hellogrowapirest.repository;

import com.lima.hellogrowapirest.domain.rega.Rega;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegaRepository extends JpaRepository<Rega, Long> {

    //Derived query
    List<Rega> findByPlantaId(Long id);

    //NativeQuery
    @Query(value = "SELECT * FROM rega r where r.planta_id = :id ", nativeQuery = true)
    List<Rega> buscarRegasPlanta(Long id);
}
