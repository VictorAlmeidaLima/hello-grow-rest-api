package com.lima.hellogrowapirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloGrowApiRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloGrowApiRestApplication.class, args);
    }

}
