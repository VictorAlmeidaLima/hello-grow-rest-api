package com.lima.hellogrowapirest.infra.security;

public record DadosTokenJWT(String token) {
}
